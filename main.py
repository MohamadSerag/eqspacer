import os
import sys
import shutil
import subprocess as sp
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from numbers import Number
from models import models
import text
from guis.mainui import Ui_MainForm
from helpers import sett, open_db_conn


single_eqp_table_ids = [5, 11, 14, 16, 19, 23]


class PlantLayoutForm(QDialog):
    def __init__(self, imgpath, parent=None):
        super().__init__(parent)
        self.label = QLabel()
        self.layout = QVBoxLayout()
        self.img_path = imgpath
        self._runtime_actions()

    def _runtime_actions(self):
        self.label.setPixmap(QPixmap(self.img_path))
        self.label.setScaledContents(True)
        self.layout.addWidget(self.label)
        self.setLayout(self.layout)
        self.setWindowTitle('LAYOUT')
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowFlags(self.windowFlags() | Qt.WindowMinMaxButtonsHint)

    def closeEvent(self, event):
        # self.dump_settings()
        super().closeEvent(event)


class MainUi(QMainWindow, Ui_MainForm):
    def __init__(self, std_model, table_model, eqp_model, eqp2_model):
        super().__init__()
        self.setupUi(self)
        self.menu_bar.setStyleSheet(text.MENU_STYLE)
        self.steps_label = QLabel()
        self.output_tbrowser.setText(models.get_std_notes(1))

        self.enppi_label = QLabel()
        logo = QPixmap('images/enppi.png')
        logo = logo.scaledToHeight(18)
        self.enppi_label.setPixmap(logo)
        self.statusBar.addWidget(self.enppi_label)
        self.statusBar.addWidget(QLabel())

        self.layout_form_cls = PlantLayoutForm
        self.std_model = std_model
        self.table_model = table_model
        self.eqp_model = eqp_model
        self.eqp2_model = eqp2_model
        self.all_notes_model = None
        self.current_std = 0

        self.eqp_model_proxy = models.create_proxy_model(self.eqp_model)
        self.eqp2_model_proxy = models.create_proxy_model(self.eqp2_model)
        self.all_notes_proxy = None

        self._map_views()
        self._connect_signals()

    def _map_tview(self, view, model):
        view.setModel(model)

        for i in range(model.columnCount()):
            if i != 1:
                view.setColumnHidden(i, True)

    def _map_cviews(self, view, model):
        view.setModel(model)
        view.setModelColumn(1)

    def _map_views(self):
        # self.std_cbox.setModel(self.std_model)
        # self.std_cbox.setModelColumn(1)

        for view, model in [
                            (self.std_cbox, self.std_model),
                            (self.table_cbox, self.table_model)
                            ]:

            self._map_cviews(view, model)

        for view, model in [
                            (self.primary_tview, self.eqp_model_proxy),
                            (self.secondary_tview, self.eqp2_model_proxy)
                            ]:

            self._map_tview(view, model)

    def _connect_signals(self):
        self.tbrowser_frame_action.triggered.connect(self.change_tbrowser_frame)
        self.exit_action.triggered.connect(self.close)
        self.about_action.triggered.connect(lambda : QMessageBox.about(self, 'APP CARD', text.ABOUT_APP))
        self.refs_action.triggered.connect(lambda : QMessageBox.about(self, 'REFERENCES INFO', text.REFS))
        # self.gen_notes_action.triggered.connect(lambda : self.output_tbrowser.setText(models.get_std_notes(1)))
        self.reset_action.triggered.connect(self.reset)
        self.layout1_action.triggered.connect(lambda : self.display_layout('images/1.png'))
        self.search_ledit.returnPressed.connect(self.search)
        self.notes_search_mode_cbox.stateChanged.connect(lambda state: self.notes_search_clicked(state))

        # for action in self.findChildren(QAction):
        #     tag = action.whatsThis()
        #     if 0 < len(tag) <= 3:
        #         print('start tables/{}.pdf'.format(tag))
        #         action.triggered.connect(lambda : sp.call('start tables/{}.pdf'.format(tag), shell=True))

        # self.gap1_action.triggered.connect(lambda : sp.call('start tables/g1.pdf', shell=True))
        # self.gap2_action.triggered.connect(lambda : sp.call('start tables/g2.pdf', shell=True))
        # self.gap3_action.triggered.connect(lambda : sp.call('start tables/g3.pdf', shell=True))
        # self.pip1_action.triggered.connect(lambda : sp.call('start tables/p1.pdf', shell=True))
        # self.pip2_action.triggered.connect(lambda : sp.call('start tables/p2.pdf', shell=True))
        # self.en1_action.triggered.connect(lambda : sp.call('start tables/en1.pdf', shell=True))
        # self.en2_action.triggered.connect(lambda : sp.call('start tables/en2.pdf', shell=True))
        # self.ex1_action.triggered.connect(lambda : sp.call('start tables/ex1.pdf', shell=True))
        # self.ex2_action.triggered.connect(lambda : sp.call('start tables/ex2.pdf', shell=True))

        self.std_cbox.currentIndexChanged.connect(lambda i: self.std_changed(i))
        self.table_cbox.currentIndexChanged.connect(lambda i: self.table_changed(i))
        self.filter1_ledit.textChanged.connect(lambda txt: self.filter1_text_changed(txt))

        self.filter2_ledit.textChanged.connect(lambda txt:
                                               self.eqp2_model_proxy.setFilterRegExp(QRegExp(txt, Qt.CaseInsensitive)))

        self.primary_tview.clicked.connect(lambda index: self.primary_eqp_selected(index))
        self.secondary_tview.clicked.connect(lambda index: self.secondary_eqp_selected(index))
        self.primary_tview.activated.connect(lambda index: self.primary_eqp_selected(index))
        self.secondary_tview.activated.connect(lambda index: self.secondary_eqp_selected(index))
        self.inc_font_button.clicked.connect(lambda x: self.change_font_size(True))
        self.dec_font_button.clicked.connect(self.change_font_size)

    def _clear_selections(self):
        self.filter1_ledit.clear()
        self.filter2_ledit.clear()
        self.eqp2_model.setQuery('select * from equipment where std_id = -1')    # Clears table view
        # self.output_tbrowser.setText(text.GENERAL_NOTES)

    def _record_values(self, record):
        values = []

        for i in range(record.count()):
            value = record.value(i).strip()

            # if i == 0 and value[:2] != '1-':
            #     value = value.partition('- ')[2]

            if value:
                values.append(conv_operator_to_html(value.replace('Select Standard', 'General Notes')))

        return values

    def filter1_text_changed(self, txt):
        if self.notes_search_mode_cbox.isChecked():
            self.all_notes_proxy.setFilterRegExp(QRegExp(txt, Qt.CaseInsensitive))

        else:
            self.eqp_model_proxy.setFilterRegExp(QRegExp(txt, Qt.CaseInsensitive))

    def search(self):
        if not self.output_tbrowser.find(self.search_ledit.text()):
            cursor = self.output_tbrowser.textCursor()
            cursor.setPosition(0)
            self.output_tbrowser.setTextCursor(cursor)
            QMessageBox.information(self, 'INFO', '<font color=#f4f4f4>No more matches found.</font>')

    def change_font_size(self, increase=False):
        font = self.output_tbrowser.font()
        font_size = font.pointSize()

        increment = 1
        if not increase:
            increment = -1

        font.setPointSize(font_size + increment)
        self.output_tbrowser.setFont(font)

    def change_tbrowser_frame(self):
        shape = QFrame.NoFrame
        shadow = QFrame.Plain

        if self.tbrowser_frame_action.isChecked():
            shape = QFrame.WinPanel
            shadow = QFrame.Sunken

        self.output_tbrowser.setFrameShape(shape)
        self.output_tbrowser.setFrameShadow(shadow)

    def display_layout(self, imgpath):
        form = self.layout_form_cls(imgpath, self)

        try:
            form.resize(sett['plant_layout_form_size'])

        except KeyError:
            pass

        form.show()

    def std_changed(self, i):
        if not self.table_cbox.isEnabled():
            self.table_cbox.setEnabled(True)

        self._clear_selections()
        self.current_std = self.std_model.record(i).value('id')
        self.table_model.setQuery('select * from StdTable where std_id = 1 or std_id = {}'.format(self.current_std))
        self.table_cbox.setCurrentIndex(0)
        self.output_tbrowser.setText(models.get_std_notes(self.current_std))

    def table_changed(self, table_i):
        table_id = self.table_model.record(table_i).value('id')

        # Select equipments of a certain table
        query = 'select * from equipment where std_id = {} and table_id = {}'.format(self.current_std, table_id)

        if table_id == 1:
            if self.current_std == 1:
                # If no std selected, display all equipments in table
                query = 'select * from equipment'

            else:
                # Select all equipments for that std
                query = 'select * from equipment where std_id = {}'.format(self.current_std)

        elif table_id == 18:
            # For pip table 18
            query = 'select * from equipment where id between 238 and 246'

        self.eqp_model.setQuery(query)
        self.eqp2_model.setQuery('select * from equipment where id = -1')
        self.filter1_ledit.setFocus()

    def primary_eqp_selected(self, index):
        # All notes search mode
        if self.notes_search_mode_cbox.checkState():
            index = self.all_notes_proxy.mapToSource(index)
            values = self._record_values(self.all_notes_model.record(index.row()))
            title = ' - '.join(values[1:])
            self.output_tbrowser.setText(text.ALL_NOTES.format(title, values[0]))
            return

        # Normal mode
        index = self.eqp_model_proxy.mapToSource(index)
        record = self.eqp_model.record(index.row())
        desc = conv_operator_to_html(record.value('desc'))
        table_id = record.value('table_id') if record.value('table_id') not in single_eqp_table_ids else '-1'

        # Table 17 has one equipment with no counterpart and can't be included in the nontabulated to avoid standard violation
        if table_id == 17:
            self.eqp2_model.setQuery('select * from equipment where std_id = -1')

        # PIP and API 2510 odd structured tables handler, eqp has relations with some of the eqps in the same table no all of them
        elif table_id in [18, 20, 21, 22]:
            # Check the spacing table for the current selection counterparts and get their ids
            eqp2_ids = models.get_counter_eqps_ids(record.value('id'))

            if eqp2_ids:
                # Limit eqp2 model to those eqps to avoid return nothing for spacing and all
                extra_clause = ' or '.join(['id = {}'.format(id_) for id_ in eqp2_ids])
                self.eqp2_model.setQuery('select * from equipment where std_id = {} '
                                     'and table_id = {} and {}'.format(record.value('std_id'), table_id, extra_clause))

        else:
            # Set eqp2 model to all the eqps from the same table
            self.eqp2_model.setQuery('select * from equipment where std_id = {} '
                                     'and table_id = {}'.format(record.value('std_id'), table_id))

        self.output_tbrowser.setText(text.EQP1_NOTES.format(models.get_title(record.value('id')),
                                                            desc,
                                                            conv_operator_to_html(record.value('notes')) or None))

    def secondary_eqp_selected(self, index):
        eqp1_index = self.eqp_model_proxy.mapToSource(self.primary_tview.currentIndex())
        eqp1_record = self.eqp_model.record(eqp1_index.row())

        index = self.eqp2_model_proxy.mapToSource(index)
        eqp2_record = self.eqp2_model.record(index.row())

        pair_record = models.get_pair_data(eqp1_record.value('id'), eqp2_record.value('id'))
        pair_dict = format_pair_data(pair_record, eqp1_record, eqp2_record)

        self.output_tbrowser.setText(text.OUTPUT_CONTEXT.format(models.get_title(eqp1_record.value('id')), **pair_dict))
        self.steps_label.setText('<font size=4 color=white>Output displayed</font>')

    def reset(self):
        self._clear_selections()
        self.std_cbox.setCurrentIndex(0)
        self.resize(sett['mainform_size'])

    def notes_search_clicked(self, checked):
        unrelated_widgets = [self.std_cbox, self.table_cbox, self.filter2_ledit, self.secondary_tview]

        if checked:
            try:
                self.reset()
                self.output_tbrowser.clear()

                # Deactivate unrelated widgets
                for w in unrelated_widgets:
                    w.setEnabled(False)

                # Detach primary eqps model, attach all notes model, and clear secondary tview
                self.primary_tview.setModel(None)
                self.eqp2_model.setQuery('select * from equipment where id = -1')
                self.all_notes_model = models.create_model('AllNotes', 'Notes', active_col=0)
                self.all_notes_proxy = models.create_proxy_model(self.all_notes_model, col=0)
                self.primary_tview.setModel(self.all_notes_proxy)

                for i in range(1, self.all_notes_model.columnCount()):
                    self.primary_tview.setColumnHidden(i, True)

            except Exception as err:
                print(err)
                self.notes_search_clicked(checked)


        else:
            self.reset()
            self.gen_notes_action.trigger()    # to display general notes

            for w in unrelated_widgets:
                w.setEnabled(True)

            self.primary_tview.setModel(None)
            self._map_tview(self.primary_tview, self.eqp_model_proxy)

    def dump_settings(self):
        sett['mainform_size'] = self.size()
        sett[self.tbrowser_frame_action.whatsThis()] = self.tbrowser_frame_action.isChecked()

    def load_settings(self):
        self.resize(sett['mainform_size'])

        if sett[self.tbrowser_frame_action.whatsThis()]:
            self.tbrowser_frame_action.trigger()

    def closeEvent(self, event):
        self.dump_settings()
        sett.write_yaml()
        super().closeEvent(event)


def create_models():
    yield models.create_model('Standard')
    yield models.create_model('StdTable', clause='where std_id = -1')
    yield models.create_model('Equipment', 'Primary Equipments')
    yield models.create_model('Equipment', 'Secondary Equipments', 'where id = -1')


def format_pair_data(pair_record, eqp1_record, eqp2_record):
    dist = pair_record.value('space')
    unit = 'm' if isinstance(dist, Number) else ''
    dist = ' '.join([str(dist), unit])

    return {'dist': dist,
            'eqp1_desc': conv_operator_to_html(eqp1_record.value('desc')),
            'eqp2_desc': conv_operator_to_html(eqp2_record.value('desc')),
            'eqp1_notes': eqp1_record.value('notes') or None,
            'eqp2_notes': eqp2_record.value('notes') or None,
            'pair_notes': pair_record.value('notes') or None}


def conv_operator_to_html(string):
    try:
        return string.replace(' < ', ' &lt; ').replace(' > ', ' &gt; ').replace(' <= ', ' &le; ').replace('?', '&deg;')

    except AttributeError:
        pass


def copy_db():
    src_path = 'data/EqSpacer.db'
    try:
        return shutil.copy(src_path, os.getenv('temp'))
    except:
        return src_path


def main():
    app = QApplication(sys.argv)
    open_db_conn(copy_db())

    sett.appname = 'EqSpacer'
    ui = MainUi(*list(create_models()))
    
    # On Windows only
    if sys.platform == 'win32':
        if os.path.exists(sett._user_config_path):

            try:
                sett.read_yaml()
                ui.load_settings()

            except (KeyError, FileNotFoundError):
                pass

    ui.show()
    app.exec_()


if __name__ == '__main__':
    main()
