from PyQt5.QtSql import *
from PyQt5.QtCore import *


class MyProxy(QSortFilterProxyModel):
    def headerData(self, section, orientation, role=None):
        if role == Qt.DisplayRole and orientation == Qt.Vertical:
            return section + 1

        else:
            return super().headerData(section, orientation, role)


def create_model(table, name=None, clause='', active_col=1):
    '''
    :param table:
    :param name: Name of the displayed column, the replacement for db column name
    :param clause:
    :param active_col: The column that will be diplayed in the table view
    :return:
    '''
    model = QSqlQueryModel()
    model.setQuery('select * from {} {}'.format(table, clause))

    if name:
        model.setHeaderData(active_col, Qt.Horizontal, name)

    return model


def create_proxy_model(model, col=1):
    proxy = MyProxy()
    proxy.setSourceModel(model)
    proxy.setFilterKeyColumn(col)
    return proxy


def get_pair_data(id1, id2):
    statement = 'SELECT space, notes FROM Spacing ' \
                'WHERE eqp1_id = {id1} and eqp2_id = {id2} ' \
                'or' \
                ' eqp1_id = {id2} and eqp2_id = {id1}'

    cursor = QSqlQuery(statement.format(id1=id1, id2=id2))
    cursor.first()
    return cursor


def get_std_notes(std_id):
    # To remove the general notes
    if std_id == 1:
        return ''

    notes = ''
    statement = 'SELECT notes FROM StdNote WHERE std_id = {std_id}'.format(std_id=std_id)
    cursor = QSqlQuery(statement)

    while cursor.next():
        notes += '{}<br><br>'.format(cursor.value(0))

    return notes


def get_title(eqp_id):
    # Get the equipment's std name, and table name
    query = 'select std_name, table_name from EqpStdTable where eqp_id = {}'.format(eqp_id)
    cursor = QSqlQuery(query)
    cursor.first()
    return '{} - {}'.format(cursor.value('std_name'), cursor.value('table_name'))


def get_eqp2_range(eqp1_id):
    query = 'select low_limit, high_limit from CounterEqpRange where eqp1_id = {}'.format(eqp1_id)
    cursor = QSqlQuery(query)
    cursor.first()
    return [cursor.value('low_limit'), cursor.value('high_limit')]

def get_counter_eqps_ids(eqp1_id):
    query = 'select eqp2_id from spacing where eqp1_id = {}'.format(eqp1_id)
    query2 = 'select eqp1_id from spacing where eqp2_id = {}'.format(eqp1_id)
    cursor = QSqlQuery(query)

    if not cursor.next():
        cursor = QSqlQuery(query2)
        cursor.first()

    counter_ids = [cursor.value(0)]
    while cursor.next():
        counter_ids.append(cursor.value(0))

    return counter_ids

