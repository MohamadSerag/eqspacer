from subprocess import call

names = ["mainui"]
for name in names:
    call("s:/interpreters/python34/Lib/site-packages/PyQt5/pyuic5 -x {0}.ui -o {0}.py".format(name),
         shell=True)
