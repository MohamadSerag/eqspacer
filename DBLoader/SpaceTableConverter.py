import xlrd as xl


wb = xl.open_workbook("20-8-2015 Update\Tables loading - PIP & UOP.xlsx")
sheet0 = wb.sheet_by_index(2)
header = sheet0.row_values(3)[2:]
output = []

for i in range(4, 21):

    row = sheet0.row_values(i)
    eqp1 = int(row[0])  #index

    for index, cell in enumerate(row[2:]):
        if cell == "X":
            continue

        eqp2 = index + 204    #index
        dist = cell
        output.append([eqp1, eqp2, dist])


import csv

with open("UOP.csv", "w", newline="") as ofile:
    writer = csv.writer(ofile)
    for row in output:
        writer.writerow(row)
