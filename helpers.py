import re
import os
import math
import yaml

			
def split_into_groups(dataset, grouplimit):
    group_count = math.ceil(len(dataset) / grouplimit)
    start_index, end_index = 0, grouplimit
    groups = []

    for i in range(group_count):
        groups.append(dataset[int(start_index) : int(end_index)])
        start_index += grouplimit
        end_index += grouplimit

    return groups


def open_db_conn(conn_str, driver="QSQLITE"):   # conn_str = filepath, for sqlite db
    from PyQt5.QtSql import QSqlDatabase
    conn = QSqlDatabase.addDatabase(driver)
    conn.setDatabaseName(conn_str)

    if not conn.open():
        print("Couldn't connect to the db!")

    else:
        return conn

########################################################################################################################
   

class Settings(dict):
    appname="MyApp"

    @property
    def _user_config_path(self):
        return os.path.join(os.getenv("APPDATA"), self.appname)

    def read_txt(self, filepath="config/config.ini", tostring=False):
        pattern = "\s*=\s+"

        with open(filepath) as config_file:
            for line in config_file:
                line = line.strip()
                
                if not line or line.startswith("#"):
                    continue
                
                key, value = re.split(pattern, line)
                
                if value.isdigit() and not tostring:
                    value = int(value)
                    
                elif value.replace(".", "").isdigit() and value.count(".") == 1 and not tostring:
                    value = float(value)
                    
                self[key] = value
                    
    def read_yaml(self):
        """Reads settings from user\appdata\..."""
        with open(os.path.join(self._user_config_path, "config.yaml")) as file:
            self.update(yaml.load(file))
        
    def write_yaml(self):
        """Writes settings to user\appdata\..."""
        if not os.path.exists(self._user_config_path):
            os.mkdir(self._user_config_path)
            
        with open(os.path.join(self._user_config_path, "config.yaml"), "w") as file:
            yaml.dump(self, file)


sett = Settings()

########################################################################################################################

class UiSettManMixin:
    def __init__(self, load_func=None, dump_func=None):
        super().__init__()
        self._load_sett_strategy = load_func
        self._dump_sett_strategy = dump_func

    def dump_settings(self, *args, **kwargs):
        self._dump_sett_strategy(self, *args, **kwargs)

    def load_settings(self, *args, **kwargs):
        self._load_sett_strategy(self, *args, **kwargs)

