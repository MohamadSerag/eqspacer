
MENU_STYLE = '''
        QMenuBar {background-color: rgb(77,77,77); color: rgb(255,255,255);border: 1px solid #000;}
        QMenuBar::item {background-color: rgb(77,77,77); color: rgb(255,255,255);}
        QMenu {background-color: rgb(77,77,77);color: rgb(255,255,255);border: 1px solid #000;}
        QMenu::item::selected {background-color: rgb(30,30,30);}'''

ABOUT_APP = '''
<font color=#f4f4f4><strong>Eq. Spacer</strong> v0.5.2<br><br>
Released: 06-04-2016<br><br>
By: Mohamed Serag/Osama Fahmy<br><br>
Copyright: Enppi, Piping SSD section</font>
'''

REFS = '''
<font color=#f4f4f4>1- Enppi Design Guide 1-EG-0314, Recommended Spacing For Refineries, Gas and Petrochemical Plants.<br><br>
2- GAP 2.5.2, Oil And Chemical Plant Layout And Spacing.<br><br>
3- PIP PNE00003, Process Unit and Offsites Layout Guide, June 2013.<br><br>
4- Exxon Mobile Design Practices, Safety In Plant Design - Equipment Spacing, Section XV-G, Dec. 2000.<br><br>
5- UOP - Plot Plan Design Criteria For Process Units - Standard Specification 9-51-5, June 2014.
'''

EQP1_NOTES = '''<font color=#f4f4f4><center><h4><font color=#FFA500>{}</font></h4></center><b>{} Notes:</b><br>{}</font>'''

ALL_NOTES = '''<font color=#f4f4f4><center><h4><font color=#FFA500>{}</font></h4></center>{}</font>'''

OUTPUT_CONTEXT = '''
<font FACE='segoe ui' color=#f4f4f4><center><h4><font color=#FFA500>{}</font></h4></center><div>Minimum spacing is from edge to edge.</div><div><br><b>Minimum Spacing</b> = {dist}</div><br>
<b>{eqp1_desc} Notes</b>:<br>
{eqp1_notes}<br><br>
<b>{eqp2_desc} Notes</b>:<br>
{eqp2_notes}<br><br>
<b>Pair Notes:</b><br>
{pair_notes}<br></font>
'''
